﻿namespace ListaNumerosWF
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnNumerosAleatorio10 = new System.Windows.Forms.Button();
            this.btnNumerosAleatorio15 = new System.Windows.Forms.Button();
            this.btnVaciar10 = new System.Windows.Forms.Button();
            this.btnVaciar15 = new System.Windows.Forms.Button();
            this.btnInvertir = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnOrdenar = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Location = new System.Drawing.Point(98, 42);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(0, 18);
            this.lblResultado.TabIndex = 1;
            // 
            // btnNumerosAleatorio10
            // 
            this.btnNumerosAleatorio10.Location = new System.Drawing.Point(55, 224);
            this.btnNumerosAleatorio10.Name = "btnNumerosAleatorio10";
            this.btnNumerosAleatorio10.Size = new System.Drawing.Size(100, 57);
            this.btnNumerosAleatorio10.TabIndex = 2;
            this.btnNumerosAleatorio10.Text = "Numeros Aleatorios";
            this.btnNumerosAleatorio10.UseVisualStyleBackColor = true;
            this.btnNumerosAleatorio10.Click += new System.EventHandler(this.btnNumerosAleatorio10_Click);
            // 
            // btnNumerosAleatorio15
            // 
            this.btnNumerosAleatorio15.Location = new System.Drawing.Point(55, 287);
            this.btnNumerosAleatorio15.Name = "btnNumerosAleatorio15";
            this.btnNumerosAleatorio15.Size = new System.Drawing.Size(100, 57);
            this.btnNumerosAleatorio15.TabIndex = 2;
            this.btnNumerosAleatorio15.Text = "Numeros Aleatorios";
            this.btnNumerosAleatorio15.UseVisualStyleBackColor = true;
            this.btnNumerosAleatorio15.Click += new System.EventHandler(this.btnNumerosAleatorio15_Click);
            // 
            // btnVaciar10
            // 
            this.btnVaciar10.Location = new System.Drawing.Point(161, 224);
            this.btnVaciar10.Name = "btnVaciar10";
            this.btnVaciar10.Size = new System.Drawing.Size(100, 57);
            this.btnVaciar10.TabIndex = 2;
            this.btnVaciar10.Text = "Vaciar10";
            this.btnVaciar10.UseVisualStyleBackColor = true;
            this.btnVaciar10.Click += new System.EventHandler(this.btnVaciar10_Click);
            // 
            // btnVaciar15
            // 
            this.btnVaciar15.Location = new System.Drawing.Point(161, 287);
            this.btnVaciar15.Name = "btnVaciar15";
            this.btnVaciar15.Size = new System.Drawing.Size(100, 57);
            this.btnVaciar15.TabIndex = 2;
            this.btnVaciar15.Text = "Vaciar15";
            this.btnVaciar15.UseVisualStyleBackColor = true;
            this.btnVaciar15.Click += new System.EventHandler(this.btnVaciar15_Click);
            // 
            // btnInvertir
            // 
            this.btnInvertir.Location = new System.Drawing.Point(267, 224);
            this.btnInvertir.Name = "btnInvertir";
            this.btnInvertir.Size = new System.Drawing.Size(100, 57);
            this.btnInvertir.TabIndex = 2;
            this.btnInvertir.Text = "Invertir";
            this.btnInvertir.UseVisualStyleBackColor = true;
            this.btnInvertir.Click += new System.EventHandler(this.btnInvertir_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(373, 224);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 57);
            this.button2.TabIndex = 2;
            this.button2.Text = "Vaciar10";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnVaciar10_Click);
            // 
            // btnOrdenar
            // 
            this.btnOrdenar.Location = new System.Drawing.Point(267, 287);
            this.btnOrdenar.Name = "btnOrdenar";
            this.btnOrdenar.Size = new System.Drawing.Size(100, 57);
            this.btnOrdenar.TabIndex = 2;
            this.btnOrdenar.Text = "Ordenar 10";
            this.btnOrdenar.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(373, 287);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 57);
            this.button4.TabIndex = 2;
            this.button4.Text = "Vaciar15";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.btnVaciar15_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 420);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.btnVaciar15);
            this.Controls.Add(this.btnOrdenar);
            this.Controls.Add(this.btnNumerosAleatorio15);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnInvertir);
            this.Controls.Add(this.btnVaciar10);
            this.Controls.Add(this.btnNumerosAleatorio10);
            this.Controls.Add(this.lblResultado);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnNumerosAleatorio10;
        private System.Windows.Forms.Button btnNumerosAleatorio15;
        private System.Windows.Forms.Button btnVaciar10;
        private System.Windows.Forms.Button btnVaciar15;
        private System.Windows.Forms.Button btnInvertir;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnOrdenar;
        private System.Windows.Forms.Button button4;
    }
}

