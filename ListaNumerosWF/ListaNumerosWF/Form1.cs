﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaNumerosWF
{
    public partial class Form1 : Form
    {
        ListaNumeros listaNumeros10;
        ListaNumeros listaNumeros15 = new ListaNumeros();
        ListaNumeros listaNumeroVacio;
        
        Random random = new Random();

        public Form1()
        {
            
            InitializeComponent();
            
        }

        private void btnNumerosAleatorio10_Click(object sender, EventArgs e)
        {
            
            listaNumeros10 = new ListaNumeros();
            lblResultado.Text = "";
            for (int i = 0; i <= 10; i++)
            {

                listaNumeros10.Insertar(random.Next(0, 99));
                lblResultado.Text += (listaNumeros10.Elemento(i)+" ");
            }

        }

        private void btnVaciar10_Click(object sender, EventArgs e)
        {
            listaNumeroVacio = new ListaNumeros();
            listaNumeros10 = listaNumeroVacio;
            lblResultado.Text = "";
        }

        private void btnVaciar15_Click(object sender, EventArgs e)
        {
            listaNumeroVacio = new ListaNumeros();
            listaNumeros15 = listaNumeroVacio;
            lblResultado.Text = "";
        }

        private void btnInvertir_Click(object sender, EventArgs e)
        {
            lblResultado.Text = "";
           // lblResultado.Text = listaNumeros15.NumeroElementos()+" total";
            for (int i = listaNumeros15.NumeroElementos()-1; i>=0 ; i--)
            {
                
                lblResultado.Text += (listaNumeros15.Elemento(i) + " ");
            }
        }

        private void btnNumerosAleatorio15_Click(object sender, EventArgs e)
        {
            
            
            lblResultado.Text = "";
            for (int i = 0; i <= 15; i++)
            {

                listaNumeros15.Insertar(random.Next(0, 99));
                lblResultado.Text += (listaNumeros15.Elemento(i) + " ");
            }

        }

        


    }
}
