﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaNumerosWF
{
    class ListaNumeros
    {

        private int NumeroDeElementos;
        private int[] tabla;

        public ListaNumeros()
        {
            NumeroDeElementos = 0;
            tabla = new int[100];
        }

        public ListaNumeros(int numero, int valor)
        {
            NumeroDeElementos = 0;
            tabla = new int[100];
            for (int i = 0; i < numero; i++)
              tabla[i] = valor;
            NumeroDeElementos = numero;            
        }
        public int NumeroElementos()
        {
            return NumeroDeElementos;
        }
        public void Insertar(int valor)
        {
            if (NumeroDeElementos < 100)
            {
                tabla[NumeroDeElementos++] = valor;
            }
        }
        public int Primero()
        {
            if (NumeroDeElementos > 0)
                return tabla[0];
            else return -1000;
        }

        public int Ultimo()
        {
            if (NumeroDeElementos > 0)
                return tabla[NumeroDeElementos - 1];
            else return -1000;
        }

        public int Sacar()
        {
            int[] tablaAux = tabla;
            int numero = tabla[0];
            for (int i = 0; i < tabla.Length; i++)
            {
                tabla[i] = tablaAux[i + 1];
            }
            NumeroDeElementos--;
            return numero;
        }

        public int Elemento(int posicion)
        {
            if (posicion <= tabla.Length && posicion >= 0)
            {
                return tabla[posicion];
            }
            else return -10000;
        }

        public int Posicion(int valor)
        {
            if (valor <= tabla.Length && valor >= 0)
            {
                return tabla[valor];
            }
            else return -1;
        }

        public Boolean Vacia()
        {
            if (tabla.Length == 0)
                return true;
            else return false;
        }

        public void Vaciar()
        {
            ListaNumeros listaNumeros = new ListaNumeros();


        }

    }


}
